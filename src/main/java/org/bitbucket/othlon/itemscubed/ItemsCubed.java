package org.bitbucket.othlon.itemscubed;


import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraftforge.client.event.ModelRegistryEvent;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import org.apache.logging.log4j.Logger;
import org.bitbucket.othlon.itemscubed.blocks.cubedBlocks;
import org.bitbucket.othlon.itemscubed.items.cubedItems;
import org.bitbucket.othlon.itemscubed.proxy.CommonProxy;

@Mod(modid = ItemsCubed.MODID, name = ItemsCubed.NAME, version = ItemsCubed.VERSION)
public class ItemsCubed {

    public static final String MODID   = "itemscubed";
    public static final String NAME    = "ItemsCubed";
    public static final String VERSION = "1.0.0";

    //proxy business
    @SidedProxy(clientSide = "org.bitbucket.othlon.itemscubed.proxy.ClientProxy", serverSide = "org.bitbucket.othlon.itemscubed.proxy.ServerProxy")
    public static CommonProxy proxy;

    @Mod.Instance
    public static ItemsCubed instance;
    public static Logger     logger;

    //initalisation jobbies

    @Mod.EventHandler
    public void preInit(FMLPreInitializationEvent event){
        proxy.preInit(event);
        System.out.println(NAME + "is loading! noice. ");
    }

    @Mod.EventHandler
    public void init(FMLInitializationEvent event) {
        proxy.init(event);
    }

    @Mod.EventHandler
    public void postInit(FMLPostInitializationEvent event) {
        proxy.postInit(event);
    }

    //registration jobs
    @Mod.EventBusSubscriber
    public static class RegistrationHandler
    {
        @SubscribeEvent
        public static void registerBlocks(RegistryEvent.Register<Block> event)
        {
            cubedBlocks.register(event.getRegistry());
        }

        @SubscribeEvent
        public static void registerItems(RegistryEvent.Register<Item> event)
        {
            cubedItems.register(event.getRegistry());
            cubedBlocks.registerItemBlocks(event.getRegistry());
        }
        @SubscribeEvent
        public static void registerModels(ModelRegistryEvent event)
        {
           cubedItems.registerModels();
           cubedBlocks.registerModels();
        }
    }

}
