package org.bitbucket.othlon.itemscubed.proxy;

//minecraft imports

import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraftforge.client.event.ModelRegistryEvent;
import net.minecraftforge.common.config.Configuration;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.network.NetworkRegistry;
import org.bitbucket.othlon.itemscubed.ItemsCubed;
import org.bitbucket.othlon.itemscubed.blocks.cubedBlocks;
import org.bitbucket.othlon.itemscubed.config.Config;

import java.io.File;

//my mod imports
//java imports

@Mod.EventBusSubscriber
public class CommonProxy {

    //config insancing
    public static Configuration config;


    //initalization business

    public void preInit(FMLPreInitializationEvent event) {
        File directory = event.getModConfigurationDirectory();
        config = new Configuration(new File(directory.getPath(), "itemscubed.cfg"));
        Config.readConfig();
    }
    public void init(FMLInitializationEvent event) {
        NetworkRegistry.INSTANCE.registerGuiHandler(ItemsCubed.instance, new GuiProxy());
    }

    public void postInit(FMLPostInitializationEvent event) {
        if (config.hasChanged()) {
            config.save();
        }
    }

    //registration business
    public void registerItemRenderer(Item item, int meta, String id){
        }

    //@SubscribeEvent
    public static void registerBlocks(RegistryEvent.Register<Block> event) {

    }

    //@SubscribeEvent
    public static void registerModels(ModelRegistryEvent event){
        cubedBlocks.registerModels();
    }
}
