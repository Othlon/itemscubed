package org.bitbucket.othlon.itemscubed.proxy;

import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.item.Item;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.client.model.obj.OBJLoader;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.relauncher.Side;
import org.bitbucket.othlon.itemscubed.ItemsCubed;

@Mod.EventBusSubscriber(Side.CLIENT)
public class ClientProxy extends CommonProxy{

    //initalizaion business
    @Override
    public void preInit(FMLPreInitializationEvent event)
    {
        super.preInit(event);

        OBJLoader.INSTANCE.addDomain(ItemsCubed.MODID);
    }

    @Override
    public void init(FMLInitializationEvent event) { super.init(event); }

    @Override
    public void postInit(FMLPostInitializationEvent event){super.postInit(event);}


    //registration business
    @Override
    public void registerItemRenderer(Item item, int meta, String id){
        ModelLoader.setCustomModelResourceLocation(item, meta, new ModelResourceLocation(ItemsCubed.MODID + ":" + id, "inventory"));

    }


}//class end
