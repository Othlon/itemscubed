package org.bitbucket.othlon.itemscubed.items;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import org.bitbucket.othlon.itemscubed.ItemsCubed;

public class cubedItemBase extends Item {

    protected String name;

    public cubedItemBase(String name)
    {
        this.name = name;
        setUnlocalizedName(name);
        setRegistryName(name);
    }

    public void registerItemModel() {
        ItemsCubed.proxy.registerItemRenderer(this, 0, name);
    }

    @Override
    public cubedItemBase setCreativeTab(CreativeTabs tab) {
        super.setCreativeTab(tab);
        return this;
    }
}
