package org.bitbucket.othlon.itemscubed.blocks;

import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;

public class MobBlock extends cubedBlockBase{

    public MobBlock(String name)
    {
        super(Material.CLOTH, name);

        setHardness(3f);
        setResistance(5f);
    }

    @Override
    public MobBlock setCreativeTab(CreativeTabs tab)
    {
        super.setCreativeTab(tab);
        return this;
    }
}
