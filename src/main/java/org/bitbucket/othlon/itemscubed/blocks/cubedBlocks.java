package org.bitbucket.othlon.itemscubed.blocks;

import net.minecraft.block.Block;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraftforge.registries.IForgeRegistry;

public class cubedBlocks {

    //init and register blocks
    public static MobBlock spider_eye_block = new MobBlock("spider_eye_block").setCreativeTab(CreativeTabs.MATERIALS);

    public static void register(IForgeRegistry<Block> registry)
    {
        registry.registerAll(spider_eye_block);
    }

    public static void registerItemBlocks(IForgeRegistry<Item> registry)
    {
        registry.registerAll( spider_eye_block.createItemBlock());
    }

    public static void registerModels()
    {
        spider_eye_block.registerItemModel(Item.getItemFromBlock(spider_eye_block));
    }


}
