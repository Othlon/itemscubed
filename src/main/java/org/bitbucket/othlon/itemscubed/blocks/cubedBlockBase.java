package org.bitbucket.othlon.itemscubed.blocks;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;
import org.bitbucket.othlon.itemscubed.ItemsCubed;

public class cubedBlockBase extends Block {

    protected String name;

    public cubedBlockBase(Material material, String name)
    {
        super(material);
        this.name = name;

        setUnlocalizedName(name);
        setRegistryName(name);
    }

    public void registerItemModel(Item itemBlock) {ItemsCubed.proxy.registerItemRenderer(itemBlock, 0, name);}

    public Item createItemBlock() {return new ItemBlock(this).setRegistryName(getRegistryName());}

    @Override
    public cubedBlockBase setCreativeTab(CreativeTabs tab)
    {
        super.setCreativeTab(tab);
        return this;
    }
}
