package org.bitbucket.othlon.itemscubed.config;

import com.google.common.collect.Maps;
import net.minecraftforge.common.config.Configuration;
import org.apache.logging.log4j.Level;
import org.bitbucket.othlon.itemscubed.ItemsCubed;
import org.bitbucket.othlon.itemscubed.proxy.CommonProxy;

import java.util.HashMap;

public class Config {

    public static HashMap<String, Boolean> manual_bool = Maps.newHashMap();

    private static final String CATEGGORY_GENERAL = "general";

    //bools for recipes

    public static boolean cube_spider_eye = true;
    //public static boolean toItems_spidereyes = true;


    public static void readConfig()
    {
        Configuration cfg = CommonProxy.config;
        try {
            cfg.load();
            initGeneralConfig(cfg);
            }
        catch (Exception e1)
            {
                ItemsCubed.logger.log(Level.ERROR, "Err loading config file", e1);
            }
        finally
            {
                if(cfg.hasChanged()) { cfg.save(); }
            }
    }

    public static void initGeneralConfig(Configuration cfg){
        cfg.addCustomCategoryComment(CATEGGORY_GENERAL, "General default configuarion");

        cube_spider_eye = cfg.getBoolean("cube_spider_eye", CATEGGORY_GENERAL,cube_spider_eye, "set false to disable spidereye block recipe");

        manual_bool.put("cube_spider_eye", cube_spider_eye);

    }
}

