package org.bitbucket.othlon.itemscubed.config;

import com.google.gson.JsonObject;
import net.minecraft.util.JsonUtils;
import net.minecraftforge.common.crafting.IConditionFactory;
import net.minecraftforge.common.crafting.JsonContext;

import java.util.function.BooleanSupplier;

public class ConfigRecipeCondition implements IConditionFactory {
    @Override
    public BooleanSupplier parse(JsonContext context, JsonObject json)
    {
        final String key = JsonUtils.getString(json, "config_key");
        return () -> {
            if (!Config.manual_bool.containsKey(key)) {
                System.out.println("Attempted to use a config against a key that does not exist in config, defaulting to enabled config is " + key);
                return true;
            }
            return Config.manual_bool.get(key);
        };
    }
}
